# AWS VPC Module

## Overview

Creates a VPC with a dynamic number of Subnets and flexibile Subnet CIDR Ranges as well as NACLs, Routing and VPC Endpoints.
The list of Subnets looks like this:

- List of Subnet Groups
  - Map of Details
    - cidrs: [List of CIDRs]
    - type: <Type: public/private/protected>

```json
[
  {
    "cidrs": [
      "10.10.0.0/24",
      "10.10.1.0/24"
    ],
    "type": "public"
  },
  {
    "cidrs": [
      "10.10.10.0/24",
      "10.10.11.0/24"
    ],
    "dedicated_network_acl": {
      "inbound_acl_rules": [
        {
          "cidr_block": "0.0.0.0/0",
          "from_port": 0,
          "protocol": "-1",
          "rule_action": "allow",
          "rule_number": 100,
          "to_port": 0
        },
        {
          "cidr_block": "192.0.0.0/8",
          "from_port": 80,
          "protocol": "-1",
          "rule_action": "deny",
          "rule_number": 200,
          "to_port": 80
        }
      ],
      "outbound_acl_rules": [
        {
          "cidr_block": "0.0.0.0/0",
          "from_port": 0,
          "protocol": "-1",
          "rule_action": "allow",
          "rule_number": 100,
          "to_port": 0
        }
      ]
    },
    "type": "private"
  },
  {
    "cidrs": [
      "10.10.20.0/24",
      "10.10.21.0/24"
    ],
    "type": "protected"
  }
]
```

## Usage

````
module "network_config_vpc_dev" {
  source = "../modules/terraform-aws-network-vpc"
  providers = {
    aws = aws.network
  }

  # General VPC Setup #
  name                                = "dev"
  type                                = "stage"
  cidr                                = "10.10.0.0/16"

  # VPC DNS #
  enable_dns_support                  = true
  enable_dns_hostnames                = true

  subnets            = [{"cidrs":["10.10.0.0/24","10.10.1.0/24"],"name":"main","type":"public"},{"cidrs":["10.10.10.0/24","10.10.11.0/24"],"dedicated_network_acl":{"inbound_acl_rules":[{"cidr_block":"0.0.0.0/0","from_port":0,"protocol":"-1","rule_action":"allow","rule_number":100,"to_port":0},{"cidr_block":"192.0.0.0/8","from_port":80,"protocol":"-1","rule_action":"deny","rule_number":200,"to_port":80}],"outbound_acl_rules":[{"cidr_block":"0.0.0.0/0","from_port":0,"protocol":"-1","rule_action":"allow","rule_number":100,"to_port":0}]},"name":"main","type":"private"},{"cidrs":["10.10.20.0/24","10.10.21.0/24"],"name":"main","type":"protected"}]
  availability_zones = 2

  # Gateways #
  create_vpc_internet_gateway = true
  create_vpc_nat_gateways     = true

  # Flow Logs #
  create_vpc_flow_logs        = false

  enable_endpoints = []

  transit_gateway_id      = ""
  transit_gateway_routing = false

  # Other #
  tags = merge(var.tags, {Environment = "dev"}, {"Key1":"Value1","Key2":"Value2"})
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.network"></a> [aws.network](#provider\_aws.network) | 4.2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc_subnets_non_private"></a> [vpc\_subnets\_non\_private](#module\_vpc\_subnets\_non\_private) | ../terraform-aws-network-subnets | n/a |
| <a name="module_vpc_subnets_private"></a> [vpc\_subnets\_private](#module\_vpc\_subnets\_private) | ../terraform-aws-network-subnets | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_default_network_acl.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_network_acl) | resource |
| [aws_default_route_table.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_route_table) | resource |
| [aws_default_security_group.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group) | resource |
| [aws_ec2_transit_gateway_route.blackholes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route) | resource |
| [aws_ec2_transit_gateway_route.tgw_routes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route) | resource |
| [aws_ec2_transit_gateway_route_table.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table) | resource |
| [aws_ec2_transit_gateway_route_table_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table_association) | resource |
| [aws_ec2_transit_gateway_route_table_propagation.tgw_propagate_to_rtbs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table_propagation) | resource |
| [aws_ec2_transit_gateway_route_table_propagation.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_route_table_propagation) | resource |
| [aws_ec2_transit_gateway_vpc_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_transit_gateway_vpc_attachment) | resource |
| [aws_flow_log.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/flow_log) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_internet_gateway.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_ram_resource_share.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [aws_vpc.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_vpc_dhcp_options.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_dhcp_options) | resource |
| [aws_vpc_dhcp_options_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_dhcp_options_association) | resource |
| [aws_availability_zones.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | A list of availability zones names or ids in the region | `number` | n/a | yes |
| <a name="input_cidr"></a> [cidr](#input\_cidr) | The primary CIDR block for the VPC. | `string` | n/a | yes |
| <a name="input_create_vpc_flow_logs"></a> [create\_vpc\_flow\_logs](#input\_create\_vpc\_flow\_logs) | Whether or not to enable VPC Flow Logs | `bool` | `false` | no |
| <a name="input_create_vpc_internet_gateway"></a> [create\_vpc\_internet\_gateway](#input\_create\_vpc\_internet\_gateway) | Controls if an Internet Gateway is created for public subnets and the related routes that connect them. | `bool` | `false` | no |
| <a name="input_create_vpc_nat_gateways"></a> [create\_vpc\_nat\_gateways](#input\_create\_vpc\_nat\_gateways) | Should be true if you want to provision NAT Gateways for each of your private networks | `bool` | `false` | no |
| <a name="input_dhcp_options_domain_name"></a> [dhcp\_options\_domain\_name](#input\_dhcp\_options\_domain\_name) | Specifies DNS name for DHCP options set (requires enable\_dhcp\_options set to true) | `string` | `""` | no |
| <a name="input_dhcp_options_domain_name_servers"></a> [dhcp\_options\_domain\_name\_servers](#input\_dhcp\_options\_domain\_name\_servers) | Specify a list of DNS server addresses for DHCP options set, default to AWS provided (requires enable\_dhcp\_options set to true) | `list(string)` | <pre>[<br>  "AmazonProvidedDNS"<br>]</pre> | no |
| <a name="input_dhcp_options_netbios_name_servers"></a> [dhcp\_options\_netbios\_name\_servers](#input\_dhcp\_options\_netbios\_name\_servers) | Specify a list of netbios servers for DHCP options set (requires enable\_dhcp\_options set to true) | `list(string)` | `[]` | no |
| <a name="input_dhcp_options_netbios_node_type"></a> [dhcp\_options\_netbios\_node\_type](#input\_dhcp\_options\_netbios\_node\_type) | Specify netbios node\_type for DHCP options set (requires enable\_dhcp\_options set to true) | `string` | `""` | no |
| <a name="input_dhcp_options_ntp_servers"></a> [dhcp\_options\_ntp\_servers](#input\_dhcp\_options\_ntp\_servers) | Specify a list of NTP servers for DHCP options set (requires enable\_dhcp\_options set to true) | `list(string)` | `[]` | no |
| <a name="input_enable_dhcp_options"></a> [enable\_dhcp\_options](#input\_enable\_dhcp\_options) | Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type | `bool` | `false` | no |
| <a name="input_enable_dns_hostnames"></a> [enable\_dns\_hostnames](#input\_enable\_dns\_hostnames) | Should be true to enable DNS hostnames in the VPC | `bool` | `true` | no |
| <a name="input_enable_dns_support"></a> [enable\_dns\_support](#input\_enable\_dns\_support) | Should be true to enable DNS support in the VPC | `bool` | `true` | no |
| <a name="input_enable_endpoints"></a> [enable\_endpoints](#input\_enable\_endpoints) | a list with endpoints need to be enabled - use the aws naming convention | `list(string)` | `[]` | no |
| <a name="input_enable_ep_private_dns"></a> [enable\_ep\_private\_dns](#input\_enable\_ep\_private\_dns) | enable private dns for vpc endpouints | `bool` | `true` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to be used on all the resources as identifier | `string` | n/a | yes |
| <a name="input_private_cidrs"></a> [private\_cidrs](#input\_private\_cidrs) | A list of private cidr ranges | `list(string)` | <pre>[<br>  "10.0.0.0/8",<br>  "172.16.0.0/12",<br>  "192.168.0.0/16"<br>]</pre> | no |
| <a name="input_share_this"></a> [share\_this](#input\_share\_this) | Whether to share this VPC | `bool` | `true` | no |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | A list of subnets inside the VPC | `list` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |
| <a name="input_tgw_attachement"></a> [tgw\_attachement](#input\_tgw\_attachement) | Wheter the VPC should be attached to a TGW or not | `bool` | `true` | no |
| <a name="input_tgw_blackhole_cidrs"></a> [tgw\_blackhole\_cidrs](#input\_tgw\_blackhole\_cidrs) | The CIDR blocks which will be routed into a blackhole | `list(string)` | <pre>[<br>  "192.168.0.0/16",<br>  "172.16.0.0/12",<br>  "10.0.0.0/8"<br>]</pre> | no |
| <a name="input_tgw_propagate_attachment"></a> [tgw\_propagate\_attachment](#input\_tgw\_propagate\_attachment) | Wheter the VPC should propagate a route to itself into the TGW-attachment | `bool` | `false` | no |
| <a name="input_tgw_propagate_to_rtbs"></a> [tgw\_propagate\_to\_rtbs](#input\_tgw\_propagate\_to\_rtbs) | A list of TGW Route tables that should get a propagation of this VPC | `list` | `[]` | no |
| <a name="input_tgw_routes"></a> [tgw\_routes](#input\_tgw\_routes) | A list of Routes for the VPC Route Table with cidr and attachement\_id | `list` | `[]` | no |
| <a name="input_transit_gateway_id"></a> [transit\_gateway\_id](#input\_transit\_gateway\_id) | ID of a Transit Gateway (if available) | `string` | `""` | no |
| <a name="input_transit_gateway_routing"></a> [transit\_gateway\_routing](#input\_transit\_gateway\_routing) | Should be true if you want to route outgoing public traffic via the shared vpc | `bool` | `false` | no |
| <a name="input_type"></a> [type](#input\_type) | The VPC Type. Could be 'stage' for staging VPC or 'shared' for a VPC shared with all other VPCs. | `string` | `"stage"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_igw_id"></a> [igw\_id](#output\_igw\_id) | The ID of the Internet Gateway |
| <a name="output_private_rtb_ids"></a> [private\_rtb\_ids](#output\_private\_rtb\_ids) | The route table ids for the private subnets |
| <a name="output_private_subnet_id_cidr_map"></a> [private\_subnet\_id\_cidr\_map](#output\_private\_subnet\_id\_cidr\_map) | A map of ID: CIDR for the Private Subnets |
| <a name="output_ram_share_arn"></a> [ram\_share\_arn](#output\_ram\_share\_arn) | ARN of the RAM Share for the main Subnets |
| <a name="output_tgw_attachment_id"></a> [tgw\_attachment\_id](#output\_tgw\_attachment\_id) | Transit Gateway VPC Azttachment ID |
| <a name="output_tgw_rtb_id"></a> [tgw\_rtb\_id](#output\_tgw\_rtb\_id) | Transit Gateway Route Table |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | The ID of the VPC |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
