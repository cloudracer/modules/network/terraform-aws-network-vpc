// Restrict default security group
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.this[0].id

  tags = {
    Name = "sg-${lower(var.name)}-default-tflz"
  }
}

// Remove all routes from default route table
resource "aws_default_route_table" "default" {
  default_route_table_id = aws_vpc.this[0].default_route_table_id

  tags = {
    Name = "rtb-${lower(var.name)}-default-tflz"
  }
}

// Default NACL
resource "aws_default_network_acl" "default" {
  default_network_acl_id = aws_vpc.this[0].default_network_acl_id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "nacl-${lower(var.name)}-default-tflz"
  }

  lifecycle {
    ignore_changes = [subnet_ids]
  }
}
