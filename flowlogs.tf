# VPC Flow Logs #
resource "aws_cloudwatch_log_group" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name              = "/aws/vpc/flow_logs/${lower(var.name)}"
  retention_in_days = "90"

  tags = merge(local.tags, { Name = lower("${var.name}") })
}

resource "aws_flow_log" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  iam_role_arn    = aws_iam_role.this[0].arn
  log_destination = aws_cloudwatch_log_group.this[0].arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.this[0].id

  tags = merge(local.tags, { Name = lower(var.name) })
}

resource "aws_iam_role" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name = "${var.name}-vpc-cloudwatchlogs-role-tflz"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = merge(local.tags, { Name = upper(var.name) })
}

resource "aws_iam_role_policy" "this" {
  count = var.create_vpc_flow_logs ? 1 : 0

  name   = "${var.name}-vpc-cloudwatchlogs-policy-tflz"
  role   = aws_iam_role.this[0].id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
