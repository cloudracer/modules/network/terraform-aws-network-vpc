resource "aws_ram_resource_share" "this" {
  count                     = var.share_this ? 1 : 0
  name                      = "ram-${var.name}-tflz"
  allow_external_principals = false

  tags = local.tags
}
