locals {
  non_private_subnets_to_map = { for subnet in var.subnets : "main_${subnet.type}" => subnet if subnet.type != "private" } # Needed because TF can't work with a list of Maps in for_each
  private_subnets_map        = { for subnet in var.subnets : "main_${subnet.type}" => subnet if subnet.type == "private" }
}

# For all Subnets that are not private
module "vpc_subnets_non_private" {
  source   = "../terraform-aws-network-subnets"
  for_each = local.non_private_subnets_to_map
  providers = {
    aws = aws
  }

  propagating_vgws = lookup(each.value, "propagating_vgws", [])

  # General info #
  vpc_id   = aws_vpc.this[0].id
  vpc_name = var.name

  # Gateway Info #
  route_to_igw            = var.create_vpc_internet_gateway
  igw_id                  = var.create_vpc_internet_gateway ? aws_internet_gateway.this.0.id : ""
  create_vpc_nat_gateways = var.create_vpc_nat_gateways
  transit_gateway_routing = var.transit_gateway_routing
  transit_gateway_id      = var.transit_gateway_id

  # RAM Share #
  ram_share_arn = var.share_this ? aws_ram_resource_share.this[0].arn : ""

  # Info from Subnet YAML #
  subnet_name           = "main"
  subnets_cidrs         = each.value.cidrs
  subnet_type           = each.value.type
  subnets_routes_to_tgw = lookup(each.value, "routes_to_tgw", [])
  dedicated_network_acl = lookup(each.value, "dedicated_network_acl", false) != false ? true : false
  inbound_acl_rules     = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "inbound_acl_rules", []) : []
  outbound_acl_rules    = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "outbound_acl_rules", []) : []
  enable_endpoints      = lookup(each.value, "endpoints", [])

  # Other #
  tags = local.tags
}

# For all Subnets that are private
module "vpc_subnets_private" {
  source   = "../terraform-aws-network-subnets"
  for_each = local.private_subnets_map
  providers = {
    aws = aws
  }

  propagating_vgws = lookup(each.value, "propagating_vgws", [])

  # General info #
  vpc_id   = aws_vpc.this[0].id
  vpc_name = var.name

  # Gateway Info #
  route_to_igw            = var.create_vpc_internet_gateway
  igw_id                  = var.create_vpc_internet_gateway ? aws_internet_gateway.this.0.id : ""
  create_vpc_nat_gateways = var.create_vpc_nat_gateways
  nat_gateways_ids        = lookup(module.vpc_subnets_non_private, "main_public", null) != null ? module.vpc_subnets_non_private["main_public"].nat_gateways_ids : []
  transit_gateway_routing = var.transit_gateway_routing
  transit_gateway_id      = var.transit_gateway_id

  # RAM Share #
  ram_share_arn = var.share_this ? aws_ram_resource_share.this[0].arn : ""

  # Info from Subnet YAML #
  subnet_name            = "main"
  subnets_cidrs          = each.value.cidrs
  subnet_type            = each.value.type
  subnets_routes_to_tgw  = lookup(each.value, "routes_to_tgw", [])
  dedicated_network_acl  = lookup(each.value, "dedicated_network_acl", false) != false ? true : false
  inbound_acl_rules      = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "inbound_acl_rules", []) : []
  outbound_acl_rules     = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "outbound_acl_rules", []) : []
  enable_endpoints       = lookup(each.value, "endpoints", [])
  create_public_dns_name = lookup(each.value, "create_public_dns_name", [])

  share_this = var.share_this

  # Other #
  tags = local.tags
}
