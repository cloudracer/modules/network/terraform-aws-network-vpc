# VPC #
output "vpc_id" {
  description = "The ID of the VPC"
  value       = concat(aws_vpc.this.*.id, [""])[0]
}

output "private_subnet_id_cidr_map" {
  description = "A map of ID: CIDR for the Private Subnets"
  value       = module.vpc_subnets_private["main_private"].subnet_id_cidr_map
}

# RAM #
output "ram_share_arn" {
  description = "ARN of the RAM Share for the main Subnets"
  value       = var.share_this ? aws_ram_resource_share.this[0].arn : ""
}

# Gateways #

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = concat(aws_internet_gateway.this.*.id, [""])[0]
}

output "tgw_rtb_id" {
  description = "Transit Gateway Route Table"
  value       = var.tgw_attachement == true && length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? aws_ec2_transit_gateway_route_table.this[0].id : null
}

output "tgw_attachment_id" {
  description = "Transit Gateway VPC Azttachment ID"
  value       = var.tgw_attachement == true && length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? aws_ec2_transit_gateway_vpc_attachment.this[0].id : null
}

output "private_rtb_ids" {
  value       = length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? module.vpc_subnets_private["main_private"].rtb_ids : null
  description = "The route table ids for the private subnets"
}
