# VPC Settings #

## General
variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
}

variable "cidr" {
  description = "The primary CIDR block for the VPC."
  type        = string
}

variable "type" {
  description = "The VPC Type. Could be 'stage' for staging VPC or 'shared' for a VPC shared with all other VPCs."
  type        = string
  default     = "stage"
}

## DNS Support
variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  type        = bool
  default     = true
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  type        = bool
  default     = true
}

# DHCP
variable "enable_dhcp_options" {
  description = "Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type"
  type        = bool
  default     = false
}

variable "dhcp_options_domain_name" {
  description = "Specifies DNS name for DHCP options set (requires enable_dhcp_options set to true)"
  type        = string
  default     = ""
}

variable "dhcp_options_domain_name_servers" {
  description = "Specify a list of DNS server addresses for DHCP options set, default to AWS provided (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = ["AmazonProvidedDNS"]
}

variable "dhcp_options_ntp_servers" {
  description = "Specify a list of NTP servers for DHCP options set (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = []
}

variable "dhcp_options_netbios_name_servers" {
  description = "Specify a list of netbios servers for DHCP options set (requires enable_dhcp_options set to true)"
  type        = list(string)
  default     = []
}

variable "dhcp_options_netbios_node_type" {
  description = "Specify netbios node_type for DHCP options set (requires enable_dhcp_options set to true)"
  type        = string
  default     = ""
}

## Gateways
variable "create_vpc_internet_gateway" {
  description = "Controls if an Internet Gateway is created for public subnets and the related routes that connect them."
  type        = bool
  default     = false
}

variable "create_vpc_nat_gateways" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  type        = bool
  default     = false
}



variable "subnets" {
  description = "A list of subnets inside the VPC"
  default     = []
}

variable "availability_zones" {
  description = "A list of availability zones names or ids in the region"
  type        = number
}

# VPC Flow Logs #

variable "create_vpc_flow_logs" {
  description = "Whether or not to enable VPC Flow Logs"
  type        = bool
  default     = false
}

variable "private_cidrs" {
  description = "A list of private cidr ranges"
  type        = list(string)
  default     = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
}

# Endpoints #
variable "enable_endpoints" {
  description = "a list with endpoints need to be enabled - use the aws naming convention"
  default     = []
  type        = list(string)
}

variable "enable_ep_private_dns" {
  description = "enable private dns for vpc endpouints"
  default     = true
  type        = bool
}

# Transit Gateway #
variable "transit_gateway_id" {
  type        = string
  description = "ID of a Transit Gateway (if available)"
  default     = ""
}

variable "tgw_blackhole_cidrs" {
  description = "The CIDR blocks which will be routed into a blackhole"
  type        = list(string)
  default     = ["192.168.0.0/16", "172.16.0.0/12", "10.0.0.0/8"]
}

variable "transit_gateway_routing" {
  description = "Should be true if you want to route outgoing public traffic via the shared vpc"
  type        = bool
  default     = false
}

variable "tgw_routes" {
  description = "A list of Routes for the VPC Route Table with cidr and attachement_id"
  default     = []
}

variable "tgw_propagate_to_rtbs" {
  description = "A list of TGW Route tables that should get a propagation of this VPC"
  default     = []
}

variable "tgw_attachement" {
  type        = bool
  description = "Wheter the VPC should be attached to a TGW or not"
  default     = true
}

variable "tgw_propagate_attachment" {
  type        = bool
  description = "Wheter the VPC should propagate a route to itself into the TGW-attachment"
  default     = false
}

# Other #
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "share_this" {
  type        = bool
  description = "Whether to share this VPC"
  default     = true
}
