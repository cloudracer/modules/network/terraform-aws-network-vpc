// Attach AWS Transit Gateway to the VPC in defined subnets
resource "aws_ec2_transit_gateway_vpc_attachment" "this" {
  // use ?: here as && is not short-circuiting
  count = var.tgw_attachement ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? 1 : 0 : 0

  subnet_ids         = module.vpc_subnets_private["main_private"].subnet_ids
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = aws_vpc.this[0].id
  # transit_gateway_default_route_table_association = false
  # transit_gateway_default_route_table_propagation = true
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  tags = merge(var.tags, local.tags, {
    Name = lower("tgw-attach-${lower(var.name)}-tflz")
  })
}

//
resource "aws_ec2_transit_gateway_route_table" "this" {
  provider = aws.network
  // use ?: here as && is not short-circuiting
  count = var.tgw_attachement == true ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? 1 : 0 : 0

  transit_gateway_id = var.transit_gateway_id

  tags = merge(var.tags, local.tags, {
    Name = lower("tgw-rtb-${lower(var.name)}-tflz")
  })
}

// Assoiciate AWS Transit Gateway Route Table to the Transit Gateway Attachment
resource "aws_ec2_transit_gateway_route_table_association" "this" {
  provider = aws.network
  count    = var.tgw_attachement == true ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? 1 : 0 : 0

  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.this[0].id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this[0].id
}

// Assoiciate AWS Transit Gateway Route Table to the Transit Gateway Attachment for propagation
resource "aws_ec2_transit_gateway_route_table_propagation" "this" {
  provider = aws.network
  count    = var.tgw_attachement == true && var.tgw_propagate_attachment ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? 1 : 0 : 0

  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.this[0].id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this[0].id
}

locals {
  tgw_propagate_to_rtbs_map = { for propagation in var.tgw_propagate_to_rtbs : md5(propagation) => propagation }
  tgw_routes_map            = { for route in var.tgw_routes : md5("${route["cidr"]}_${route["attachement_id"]}") => route }
}

# Propagate to other RTBs
resource "aws_ec2_transit_gateway_route_table_propagation" "tgw_propagate_to_rtbs" {
  // use ?: here as && is not short-circuiting
  for_each = var.tgw_attachement == true ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? local.tgw_propagate_to_rtbs_map : {} : {}

  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.this[0].id
  transit_gateway_route_table_id = each.value
}

# Custom Routes for VPC Route Table
resource "aws_ec2_transit_gateway_route" "tgw_routes" {
  provider = aws.network
  // use ?: here as && is not short-circuiting
  for_each = var.tgw_attachement == true ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" && length(var.tgw_routes) > 0 ? local.tgw_routes_map : {} : {}

  destination_cidr_block         = each.value["cidr"]
  transit_gateway_attachment_id  = each.value["attachement_id"]
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this[0].id
}

# Blackhole Routes
// Route all unspecified RFC1918 Network to a Blackhole
// 10.0.0.0/8 / 172.16.0.0/12 / 192.168.0.0/16
// In case of an overlapping CIDR please define the VAR.BLACKHOLE_CIDRS in your module definition
resource "aws_ec2_transit_gateway_route" "blackholes" {
  // use ?: here as && is not short-circuiting
  for_each = var.tgw_attachement == true ? length(module.vpc_subnets_private["main_private"].subnet_ids) > 0 && var.cidr != "" ? toset(var.tgw_blackhole_cidrs) : [] : []

  destination_cidr_block         = each.value
  blackhole                      = true
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this[0].id

  depends_on = [
    aws_ec2_transit_gateway_route_table.this[0]
  ]
}

# // Add routes for the SHARED VPC to the current AWS Transit Gateway Route Table
# resource "aws_ec2_transit_gateway_route" "to_shared_vpc" {
#   count = var.workspace == "shared" ? 0 : 1

#   destination_cidr_block         = var.vpc_cidr_shared
#   transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.shared.0.id
#   transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.this.id

#   depends_on = [
#     aws_ec2_transit_gateway_route_table.this
#   ]
# }
