# VPC Setup #
resource "aws_vpc" "this" {
  count = var.cidr != "" ? 1 : 0

  cidr_block           = var.cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = merge(local.tags, { Name = "vpc-${lower(var.name)}-tflz" })
}

# DHCP Options Set #
resource "aws_vpc_dhcp_options" "this" {
  count = var.enable_dhcp_options ? 1 : 0

  domain_name          = var.dhcp_options_domain_name
  domain_name_servers  = var.dhcp_options_domain_name_servers
  ntp_servers          = var.dhcp_options_ntp_servers
  netbios_name_servers = var.dhcp_options_netbios_name_servers
  netbios_node_type    = var.dhcp_options_netbios_node_type

  tags = merge(local.tags, { Name = "dopt-${lower(var.name)}-tflz" })
}

# DHCP Options Set Association #
resource "aws_vpc_dhcp_options_association" "this" {
  count = var.enable_dhcp_options ? 1 : 0

  vpc_id          = aws_vpc.this[0].id
  dhcp_options_id = aws_vpc_dhcp_options.this[0].id
}

# Internet Gateway #
resource "aws_internet_gateway" "this" {
  count = var.create_vpc_internet_gateway ? 1 : 0

  vpc_id = aws_vpc.this[0].id

  tags = merge(local.tags, { Name = "igw-${lower(var.name)}-tflz" })
}
